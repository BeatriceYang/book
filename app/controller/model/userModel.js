var dbConn  = require('../../db/db');

const User = {};

User.getByName = (name) => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM user where name = ? ", name, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
};

  
module.exports = User;