var dbConn  = require('../../db/db');

const BookType = function(bookType) {
    this.id = bookType.id;
    this.name = bookType.name;
  };

  BookType.getAll = () => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM bookType", (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  BookType.getById = (id) => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM bookType where id = ? ", id, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  BookType.getByName = (name) => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM bookType where name = ? ", name, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  BookType.getIdOrName = (id, name) => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM bookType where id=? or name=?", [id, name], (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  BookType.add = (bookType) => {
    return new Promise((resolve, reject) => {
        dbConn.query("INSERT INTO bookType SET ?", bookType,  (err, res) => {
          if (err) reject(err);
          else resolve(res);
        });
    });
  };

  BookType.delete = (id) => {
    return new Promise((resolve, reject) => {
        dbConn.query("DELETE FROM bookType WHERE id =?", id, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  BookType.update = (bookType) => {
    return new Promise((resolve, reject) => {
        dbConn.query("UPDATE author SET name=? where id=?", [bookType.name, bookType.id], (err, res) => {
          if (err) reject(err);
          else resolve(res);
        });
    });
  };

  
module.exports = BookType;