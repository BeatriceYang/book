var dbConn  = require('../../db/db');

const Book = function(book) {
    this.id = book.id;
    this.name = book.name;
    this.authId= book.authId;
    this.bookType= book.bookType;
    this.language= book.language;
    this.status= book.status;
    this.description= book.description;
};

Book.add = (book) => {
    return new Promise((resolve, reject) => {
        dbConn.query("INSERT INTO book SET ?", book,  (err, res) => {
          if (err) reject(err);
          else resolve(res);
        });
    });
};
//delete---------------------------------------------------------------------------------------------------
Book.setToDelete = (id) => {
  return new Promise((resolve, reject) => {
      dbConn.query( "UPDATE book SET status = '-1' WHERE id = ?", id,  (err, res) => {
        if (err) reject(err);
        else resolve(res);
      });
  });
};

Book.deleteBatch = () => {
  return new Promise((resolve, reject) => {
      dbConn.query( "DELETE FROM book WHERE status = '-1'",  (err, res) => {
        if (err) reject(err);
        else resolve(res);
      });
  });
};

//update----------------------------------------------------------------------------------------------------------
Book.update = (book) => {
  return new Promise((resolve, reject) => {
    var sql = "UPDATE book SET";

    var parames = [];
    if(book.name != undefined && book.name.length > 0){
      parames.push(" name = '"+book.name+"' ");
    }
    if(book.language != undefined && book.language.length > 0){
      parames.push(" language = '"+book.language+"' ");
    }
    if(book.description != undefined && book.description.length > 0){
      parames.push(" description = '"+book.description+"' ");
    }
    for(var i = 0; i<parames.length; i++){
      if (i==(parames.length-1)){
          sql+=parames[i];
      }else{
          sql+=parames[i]+",";
      }
    }
    sql+="WHERE id ="+book.id;

    dbConn.query(sql,  (err, res) => {
        if (err) reject(err);
        else resolve(res);
    });
  });
};

Book.active = (id) => {
  return new Promise((resolve, reject) => {
      dbConn.query( "UPDATE book SET status = '1' WHERE id = ?", id,  (err, res) => {
        if (err) reject(err);
        else resolve(res);
      });
  });
};

//----------------------------------------------------------------------------------------------------------
Book.search = (obj) => {
  var sql = "SELECT * FROM book";
  var parames = [];
  if(obj.id != undefined){
      parames.push(" id="+obj.id+" ");
  }
  if(obj.name != undefined && obj.name.length > 0){
      parames.push(" name like '%"+obj.name+"%' ");
  }
  if(obj.authId != undefined){
      parames.push(" authId="+obj.authId+" ");
  }
  if(obj.bookType != undefined && obj.bookType.length > 0){
      parames.push(" bookType='"+obj.bookType+"' ");
  }
  if(obj.status != undefined && obj.status.length > 0){
      parames.push(" status='"+obj.status+"' ");
  }
  if(obj.lastUpdateFrom != undefined){
    parames.push(" lastUpdate >='"+obj.lastUpdateFrom+"'");
  }
  if(obj.lastUpdateTo != undefined){
    parames.push(" lastUpdate <='"+obj.lastUpdateTo+"'");
  }
  for(var i = 0; i<parames.length; i++){
    if(i==0){
        sql+=" where "+parames[i];
    }else{
        sql+=" and "+parames[i];
    }
  }
  if(obj.from != undefined){
    sql+=" limit "+obj.from;
    if(obj.offset != undefined){
      sql+=" ,"+obj.offset;
    }
  }
  console.log(sql);

  return new Promise((resolve, reject) => {
      dbConn.query(sql,  (err, res) => {
        if (err) reject(err);
        else resolve(res);
      });
  });
};

Book.getByName = (name) => {
  return new Promise((resolve, reject) => {
      dbConn.query("SELECT * FROM book where name = ? ", name,  (err, res) => {
        if (err) reject(err);
        else resolve(res);
      });
  });
};

Book.getBookTypeById = (id) => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM bookType where id = ? ", id, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
};
 
module.exports = Book;