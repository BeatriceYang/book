var dbConn  = require('../../db/db');

const Author = function(author) {
    this.id = author.id;
    this.name = author.name;
    this.country= author.country;
    this.email= author.email;
    this.connect= author.connect;
  };


  Author.add = (author) => {
    return new Promise((resolve, reject) => {
        dbConn.query("INSERT INTO author SET ?", author,  (err, res) => {
          if (err) reject(err);
          else resolve(res);
        });
    });
  };
  Author.update = (author) => {
    return new Promise((resolve, reject) => {
      var sql = "UPDATE author SET";
      var parames = [];
      if(author.name != undefined && author.name.length > 0){
        parames.push(" name = '"+author.name+"' ");
      }
      if(author.country != undefined && author.country.length > 0){
        parames.push(" country = '"+author.country+"' ");
      }
      if(author.email != undefined && author.email.length > 0){
        parames.push(" email = '"+author.email+"' ");
      }
      if(author.connect != undefined && author.connect.length > 0){
        parames.push(" connect = '"+author.connect+"' ");
      }
      for(var i = 0; i<parames.length; i++){
        if (i==(parames.length-1)){
            sql+=parames[i];
        }else{
            sql+=parames[i]+",";
        }
      }
      sql+="WHERE id ="+author.id;
      console.log(sql);



        dbConn.query( sql,  (err, res) => {
          if (err) reject(err);
          else resolve(res);
        });
    });
  };

  Author.delete = (id) => {
    return new Promise((resolve, reject) => {
        dbConn.query("DELETE FROM author WHERE id = ?", id,  (err, res) => {
          if (err) reject(err);
          else resolve(res);
        });
    });
  };

  Author.search = (author) => {
    return new Promise((resolve, reject) => {

        console.log(author);
        var sql = "SELECT * FROM author";
        var parames = [];
        if(author.id != undefined){
            parames.push(" id="+author.id+" ");
        }
        if(author.name != undefined && author.name.length > 0){
            parames.push(" name like '%"+author.name+"%' ");
        }
        if(author.country != undefined && author.country.length > 0){
            parames.push(" country like'%"+author.country+"%' ");
        }
        if(author.email != undefined && author.email.length > 0){
            parames.push(" email='"+author.email+"' ");
        }
        if(author.connect != undefined && author.connect.length > 0){
            parames.push(" connect='"+author.connect+"' ");
        }
        console.log(parames);
    
        for(var i = 0; i<parames.length; i++){
            if(i==0){
                sql+=" where "+parames[i];
            }else{
                sql+=" and "+parames[i];
            }
        }
        console.log(sql);

        dbConn.query(sql, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  Author.getAll = () => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM author", (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  Author.getByMail = (email) => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM author where email = ?", [email], (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };

  Author.getById = (id) => {
    return new Promise((resolve, reject) => {
        dbConn.query("SELECT * FROM author where id = ? ", id, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
        });
    });
  };
  
module.exports = Author;