const Author = require("./model/authorModel.js");
const Book = require("./model/bookModel.js");
var express = require('express');
var router = express.Router();
var auth = require('../auth/auth.js');

router.post('/addAuthor',auth.authMidWare, function(req, res, next) {    
    var author = new Author({
        id: req.body.id,
        name: req.body.name,
        country: req.body.country,
        email: req.body.email,
        connect:req.body.connect,
    });
    console.log("addAuthor----------------------------------------------");

    if(  !('name' in req.body) || !('country' in req.body) 
        || !('email' in req.body) || !('connect' in req.body) ){
        res.send({"status": "01"});
    }else if( req.body.name.length === 0 || req.body.country.length === 0
        || req.body.email.length === 0 || req.body.connect.length === 0) {
        res.send({"status": "01"});
    }else{
        Author.getByMail(author.email).then(results => {
            if(results.length > 0){
              throw "02";
            }
            return Author.add(author);
          }).then(results => {
              res.send({"status":"success"});
          }).catch(err => {
              console.log(err);
              if(err == "02" || err == "01"){
                  res.send({"status":err});
              }else{
                  res.send({"status":"00"});
              }
        });
    }
});

router.post('/modifyAuthor',auth.authMidWare, async function(req, res, next) {
    console.log('/modifyAuthor----------------------------------------------');
    var author = new Author({
        id: req.body.id,
        name: req.body.name,
        country: req.body.country,
        email: req.body.email,
        connect:req.body.connect,
    });
    try{
        if( !('id' in req.body)  ){
            res.send({"status": "01"});
        }else if(req.body.id.length === 0) {
            res.send({"status": "01"});
        }else if( !(req.body.name!=undefined || req.body.country!=undefined || 
            req.body.email!=undefined || req.body.connect!=undefined) ) {
            res.send({"status": "01"});
        }else{
            var authors = await Author.getById(req.body.id);
            if(authors.length < 1){
                throw "02";
            }

            var authorMails = await Author.getByMail(author.email);
            if(authorMails.length > 0){
                throw "03";
            }

            await Author.update(author);
            res.send({"status":"success"});
        }
    }catch(err){
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
            res.send({"status":err});
        }else{
            res.send({"status":"00"});
        }
    }
});

router.post('/deleteAuthor',auth.authMidWare, async function(req, res, next) {
    console.log("deleteAuthor----------------------------------------------");
    try{
        if( !('id' in req.body)  ){
            res.send({"status": "01"});
        }else if(req.body.id.length === 0) {
            res.send({"status": "01"});
        }else{
            var authors = await Author.getById(req.body.id);
            if(authors.length < 1){
                throw "02";
            }

            var books = await Book.search({authId:req.body.id});
            if(books.length > 0){
                throw "03";
            }

            await Author.delete(req.body.id);
            res.send({"status":"success"});
        }
    }catch(err){
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
            res.send({"status":err});
        }else{
              res.send({"status":"00"});
        }
    }
});

router.post('/searchAuthor',auth.authMidWare, async function(req, res, next){    
    console.log("searchAuthor----------------------------------------------");
    var author = new Author({
        id: req.body.id,
        name: req.body.name,
        country: req.body.country,
        email: req.body.email,
        connect:req.body.connect,
    });
    try{
        var authors = await Author.search(author);
        res.send({
            "status":"success",
            "data":authors
        });
    }catch(err){
        console.log(err);
        res.send({"status":"00"});
    }
});

module.exports = router;