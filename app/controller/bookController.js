const Author = require("./model/authorModel.js");
const Book = require("./model/bookModel.js");
var express = require('express');
var router = express.Router();
// var redisClinet = require("../db/redisClient.js");
var auth = require('../auth/auth.js');

//------------------------------------------------------------------------------------------
router.post('/addBook', auth.authMidWare,  async function(req, res, next) {  
    var book = new Book({
        id: req.body.id,
        name: req.body.name,
        authId: req.body.authId,
        bookType: req.body.bookType,
        language: req.body.language,
        status: '0',
        description: req.body.description
    });
    
    try {
        console.log('/addBook------------------------------------------');
        if(  !('name' in req.body) || !('authId' in req.body) || !('bookType' in req.body) 
            || !('language' in req.body) ||  !('description' in req.body) ){
            res.send({"status": "01"});
        }else if( req.body.name.length === 0 || req.body.bookType.length === 0
            || req.body.language.length === 0 || req.body.description.length === 0) {
            res.send({"status": "01"});
        }else{
            var booksTypes = await Book.getBookTypeById(req.body.bookType);
            if(booksTypes.length < 1){
                throw "03";
            }

            var authors = await Author.getById(req.body.authId);
            if(authors.length < 1){
                throw "04";
            }

            var books = await Book.getByName(req.body.name);
            if(books.length > 0){
                throw "02";
            }
            await Book.add(book);
            res.send({"status":"success"});
        }
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
            res.send({"status":err});
        }else{
            res.send({"status":"00"});
        }
     }
})
//------------------------------------------------------------------------------------------.
router.post('/searchBook', auth.authMidWare, async function(req, res, next) {    
    console.log('/searchBook--------------------------------------- ');
    var bookObj = {
        id: req.body.id,
        name: req.body.name,
        authId: req.body.authId,
        bookType: req.body.bookType,
        status: req.body.status,
        description: req.body.description,
        lastUpdateFrom: req.body.lastUpdateFrom,
        lastUpdateTo: req.body.lastUpdateTo,
        from: req.body.from,
        offset: req.body.offset
    };
    try {
        console.log('/searchBook------------------------------------------');
        var books = await Book.search(bookObj);
        res.send({"status":"success", "data":books});
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
            res.send({"status":err});
        }else{
            res.send({"status":"00"});
        }
     }
});
//delete--------------------------------------------------------
router.post('/deleteBook', auth.authMidWare,  async function(req, res, next) {   
    try {
        console.log('/deleteBook.................................');
        if(  !('id' in req.body)  ){
            res.send({"status": "01"});
        }else{
            var obj = {id:req.body.id};
            var books =  await Book.search(obj);
            if(books.length < 1){
                throw "02";
            }
            await Book.setToDelete(req.body.id);
            res.send({"status":"success"});
        }
    
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
            res.send({"status":err});
        }else{
            res.send({"status":"00"});
        }
     }
})
router.post('/deleteBookBatch', auth.authMidWare,  async function(req, res, next){
    try {
        console.log('/deleteBookBatch.................................');
        await Book.deleteBatch();
        res.send({"status":"success"});
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){ 
            res.send({"status":err});
        }else{
            res.send({"status":"00"});
        }
     }
})

//update------------------------------------------------------------------------------------------
router.post('/modifyBook', auth.authMidWare,  async function(req, res, next) {
    console.log('/modifyBook.................................');
    var book = new Book({
        id: req.body.id,
        name: req.body.name,
        language: req.body.language,
        description: req.body.description
    });
    
    try {
        if( !('id' in req.body) ){
            res.send({"status": "01"});
        }else if( req.body.id.length === 0 ) {
            res.send({"status": "01"});
        }else if( !(req.body.name!=undefined || req.body.language!=undefined || req.body.description!=undefined) ) {
            res.send({"status": "01"});
        }else{
            var obj = {id:req.body.id};
            var books =  await  Book.search(obj);
            if(books.length < 1){
                throw "02";
            }
            await Book.update(book);
            res.send({"status":"success"});
        }
    
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
            res.send({"status":err});
        }else{
            res.send({"status":"00"});
        }
     }
})

router.post('/activeBook', auth.authMidWare,   async function(req, res, next) {  
    try {
        console.log('/activeBook.................................');
        if(  !('id' in req.body)  ){
            res.send({"status": "01"});
        }else{
            var obj = {id:req.body.id};
            var books =  await  Book.search(obj);
            if(books.length < 1){
                throw "02";
            }
            await Book.active(req.body.id);
            res.send({"status":"success"});
        }
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
            res.send({"status":err});
        }else{
            res.send({"status":"00"});
        }
     }
})

module.exports = router;
