var express = require('express');
var router = express.Router();
// var dbConn  = require('../db/db');
const Book = require("./model/bookModel.js");
const BookType = require("./model/bookTypeModel.js");
var auth = require('../auth/auth.js');

//------------------------------------------------------------------------------------------
router.post('/addBookType',auth.authMidWare,  async function(req, res, next) {     
    console.log("addBookType----------------------------");
    try {
        if( !('id' in req.body) || !('name' in req.body)  ){
            res.send({"status": "01"});
        }else if(req.body.id.length === 0 || req.body.name.length === 0) {
            res.send({"status": "01"});
        }else{
            var form_data = {
                id: req.body.id,
                name: req.body.name
            }
            var bookTypes = await BookType.getIdOrName(req.body.id, req.body.name);
            if (bookTypes.length > 0) {
                console.log(bookTypes);
                throw "02";                
            }
            await BookType.add(form_data);
            res.send({"status": "success"});
        } 
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
                  res.send({"status":err});
        }else{
                  res.send({"status":"00"});
        }
     }
})

//------------------------------------------------------------------------------------------
router.post('/deleteBookType',auth.authMidWare,  async function(req, res, next) {
    console.log("deleteBookType----------------------------");
    try {
        if( !('id' in req.body)  ){
            res.send({"status": "01"});
        }else if(req.body.id.length === 0) {
            res.send({"status": "01"});
        }else{
            var id = req.body.id;
            var books = await Book.search({bookType:id});
            if(books.length > 0){
                throw "02";
            }
            await BookType.delete(id);
            res.send({"status": "success"})
        } 
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
                  res.send({"status":err});
        }else{
                  res.send({"status":"00"});
        }
     }
})

//------------------------------------------------------------------------------------------
router.post('/showAllBookType',auth.authMidWare,  async function(req, res, next){
    console.log('/showAllBookType-----------------------------------------');
    try {
        var bookTypes = await BookType.getAll();
        res.send({"status": "success", "data":bookTypes});
     }catch (e) {
        res.send({"status": "00"});
     }
})

//------------------------------------------------------------------------------------------callback hell
router.post('/modifyBookType',auth.authMidWare,  async function(req, res, next){
    console.log("modifyBookType---------------------------------------");

    var bookType = new BookType({
        id: req.body.id,
        name: req.body.name
    });

    try {
        if( (req.body.id == undefined) || !('name' in req.body)  ){
            res.send({"status": "01"});
        }else if(req.body.id.length === 0 || req.body.name.length === 0) {
            res.send({"status": "01"});
        }else{
            var bookTypeById_List = await BookType.getById(req.body.id);
            if(bookTypeById_List < 1){
                throw "02";
            }

            var bookTypeByName_List = await BookType.getByName(req.body.name);
            if(bookTypeByName_List >0){
                throw "03";
            }

            await BookType.update(bookType);
            res.send({"status": "success"});
        }
     }catch (err) {
        console.log(err);
        if(err == "01" ||  err == "02" || err == "03" || err == "04"){
                  res.send({"status":err});
        }else{
                  res.send({"status":"00"});
        }
     }
})

module.exports = router;
