var redisClinet = require("../db/redisClient.js");
var crypto = require("crypto");

const auth = {};

auth.sec_30 = 30;

auth.authMidWare = async (req, res, next)=>{//審核token
    // console.log(req.headers);
    try{
        var token = req.headers.token;
        var exists = await redisClinet.exists(token);
        var theUser = await redisClinet.get(token);
        var ttl = await redisClinet.ttl(token);

        console.log(theUser+" exists:"+exists+", ttl:"+ttl);
    
        if(exists){
            await redisClinet.persist(token);
            await redisClinet.expire(token, auth.sec_30);
            next();
            return;
        }else{
            res.send({"status":"error token!"});
        }
    }catch(err){
        console.log(err);
        res.send({"status":err.toString()});
    }
}

auth.hashEncode = async (account) => {//產生token
    return new Promise((resolve, reject) => {
            var content = "user"+account+"date"+new Date().getTime();
            console.log(content);

            var hash = crypto.createHash("sha256");
            hash.update(content);
            var output;
            output = hash.digest("hex"); 
            console.log(output);

            resolve(output);
    });
  };

module.exports = auth;