// const Author = require("./model/authorModel.js");
var express = require('express');
var router = express.Router();
var User = require("../controller/model/userModel.js");

const auth = require("./auth.js");
const redisClient = require("../db/redisClient.js");


router.post('/login', async function(req, res, next){//檢查帳密正確性，並生成token
    console.log("login----------------------------------------------");
    // console.log(req.body);
    var user = {
        account: req.body.account,
        pw: req.body.pw
    };

    try{
        var userList = await User.getByName(user.account);
        console.dir(userList);

        if( userList.length > 0 ){
            if(user.pw == userList[0].pw){
                var token = await auth.hashEncode(user.account);

                await redisClient.set(token, user.account);
                await redisClient.expire(token, auth.sec_30);

                res.send({
                    "status":"success",
                    "token":token
                 });
                return;
            }
        }
        res.send({"status":"error password"});
    }catch(err){
        console.log(err);
        res.send({"status":"00"});
    }

});

module.exports = router;