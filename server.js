const express = require("express");
const bodyParser = require("body-parser");

const app = express();

var loginRouter = require('./app/auth/LoginController');
var bookTypeRouter = require('./app/controller/bookTypeController');
var authorTypeRouter = require('./app/controller/authorController');
var bookRouter = require('./app/controller/bookController');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.get("/", (req, res) => {
//     res.json({message:"hello Darren Project!!"});
// });

app.use('/firstAp/auth', loginRouter);
app.use('/firstAp/bookType', bookTypeRouter);
app.use('/firstAp/author', authorTypeRouter);
app.use('/firstAp/book', bookRouter);

app.listen(8080, () => {
    console.log("running on port 8080!?");
});