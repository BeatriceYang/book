const assert = require('assert');
const chai = require('chai');
const expect = require('chai').expect;
const supertest = require('supertest');
const api = require('./api.js');
const testingDate = require('./config/testingData.js');
// const should = chai.should();
// var delay = 50;

//done代表一個測項目結束，有done就得等done
describe('bookType testing----------------------------------------------', function() {
  var token = "";
  //config-----------------------------------------------
  const url = supertest('http://192.168.56.2:8080/firstAp');
  var account = testingDate.account;
  var bookType2Add =testingDate.bookType2Add;

  before('#login', (done) => {
    api.query(url, "", "/auth/login", account).then((body) => {
      expect(body.status).to.be.equal('success');
      token = body.token;
      done();
    });
  });
  
  it('#addBookType', (done) => {
    api.query(url, token, "/bookType/addBookType", bookType2Add).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#modifyBookType', (done) => {
    var bookType2Mod = {
      "id":bookType2Add.id,
      "name":testingDate.bookType2Mod.name
    };
    api.query(url, token, '/bookType/modifyBookType', bookType2Mod).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#check Add bookType', (done) => {
    api.query(url, token, '/bookType/showAllBookType', {}).then((body) => {
      expect(body.status).to.be.equal('success');
      body.data.forEach(data=>{
        if(data.id == bookType2Add.id){
          console.log("id is "+bookType2Add.id);
          done();
        }
      });
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#deleteBookType', (done) => {
    api.query(url, token, "/bookType/deleteBookType" , bookType2Add).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

});