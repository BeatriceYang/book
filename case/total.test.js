const assert = require('assert');
const chai = require('chai');
const expect = require('chai').expect;
const supertest = require('supertest');
const api = require('./api.js');
const testingDate = require('./config/testingData.js');
// const should = chai.should();
// var delay = 50;


describe('total testing----------------------------------------------', function() {
  var token = "";
  //config-----------------------------------------------
  var url = supertest('http://192.168.56.2:8080/firstAp');
  var account = testingDate.account;
  var author2Add =testingDate.author2Add;
  var bookType2Add =testingDate.bookType2Add;
  var book2Add =testingDate.book2Add;

  before('#login', (done) => {
    api.query(url, "", "/auth/login", account).then((body) => {
      expect(body.status).to.be.equal('success');
      token = body.token;
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#------------------------Author------------------------------', (done) => {
    done();
  });
 

  it('#addAuthor', (done) => {
    api.query(url, token, "/author/addAuthor", author2Add).then((body) => {
      var _this = this;//let 與then共享同一個this
      _this.someone = "john";
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });;
  });

  it('#check Add', (done) => {
    api.query(url, token, '/Author/searchAuthor', {}).then((body) => {
      expect(body.status).to.be.equal('success');
      body.data.forEach(data=>{
        if(data.name == author2Add.name){
          author2Add.id = data.id;
          console.log("id is "+author2Add.id);
          done();
        }
      });
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#modifyAuthor', (done) => {
    var author2Mod = {
      "id":author2Add.id,
      "connect":testingDate.author2Mod.connect
    };
    api.query(url, token, '/Author/modifyAuthor', author2Mod).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#check mod', (done) => {
    api.query(url, token, '/Author/searchAuthor', {}).then((body) => {
      expect(body.status).to.be.equal('success');
      body.data.forEach(data=>{
        if(data.name == author2Add.name){
          console.log(data);
          done();
        }
      });
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#------------------------BookType------------------------------', (done) => {
   done();
  });


  it('#addBookType', (done) => {
    api.query(url, token, "/bookType/addBookType", bookType2Add).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#modifyBookType', (done) => {
    var bookType2Mod = {
      "id":bookType2Add.id,
      "name":testingDate.bookType2Mod.name
    };
    api.query(url, token, '/bookType/modifyBookType', bookType2Mod).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#check Add bookType', (done) => {
    api.query(url, token, '/bookType/showAllBookType', {}).then((body) => {
      expect(body.status).to.be.equal('success');
      body.data.forEach(data=>{
        if(data.id == bookType2Add.id){
          console.log("id is "+bookType2Add.id);
          done();
        }
      });
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#------------------------Book------------------------------', (done) => {
    done();
   });

   it('#addBook', (done) => {
      book2Add.authId = author2Add.id;
      api.query(url, token, "/book/addBook", book2Add).then((body) => {
        expect(body.status).to.be.equal('success');
        done();
      }).catch(err=>{
        console.log(err);
      });
   });

   it('#get BookId', (done) => {
    book2Add.authId = author2Add.id;
    api.query(url, token, "/book/searchBook", {name:book2Add.name}).then((body) => {
      expect(body.status).to.be.equal('success');
      body.data.forEach(data=>{
          console.log("book id is "+data.id);
          book2Add.id = data.id;
          done();
      });
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#modifyBook', (done) => {
    var book2Mod = {
      "id":book2Add.id,
      "name":testingDate.book2Mod.description
    };
    api.query(url, token, "/book/modifyBook", book2Mod).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#activeBook', (done) => {
    book2Add.authId = author2Add.id;
    api.query(url, token, "/book/activeBook", {id:book2Add.id}).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#set Book to -1', (done) => {
    api.query(url, token, "/book/deleteBook", {id:book2Add.id}).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#------------------------delete------------------------------', (done) => {
    done();
  });

   it('#delete Book batch', (done) => {
    api.query(url, token, "/book/deleteBookBatch", {}).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#deleteBookType', (done) => {
    api.query(url, token, "/bookType/deleteBookType" , bookType2Add).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#deleteAuthor', (done) => {
    var _this = this;//let 與then共享同一個this
    console.log(_this.someone);
    api.query(url, token, "/author/deleteAuthor" , author2Add).then((body) => {

      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

});