
  exports.query = function (url, token, meth, data) {
    return url.post(meth) //
    .set('token', token)
    .send(data)
    .expect(200)
    .then(function (response) {
        if (!response) throw new Error('update client url error');
        //if (response.body.status != 'success') throw new Error('response status must be success');

        return response.body;
      })
      .catch(function (err) {
        throw err;
      });
  };