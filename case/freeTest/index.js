const Proxy = require('./proxy.js');
const proxy = new Proxy();

exports.heroIs = function () {
  it('invoke get economy protect----', function (done) {
    var _this = this;
    proxy
      .heroIs()
      .then(function (body) {//這裡的this，能連到呼叫者的this去
        _this.body = body;
        _this.someOne = "JohnCena☆"
        _this.coolGuy = "安安"
        done();
      })
      .catch(function (error) {
        console.log('測試擷取錯誤：' + error);
      });
  });
};
