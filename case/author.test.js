const assert = require('assert');
const chai = require('chai');
const expect = require('chai').expect;
const supertest = require('supertest');
const api = require('./api.js');
const testingDate = require('./config/testingData.js');
// const should = chai.should();
// var delay = 50;

//done代表一個測項目結束，有done就得等done
describe('author testing----------------------------------------------', function() {
  var token = "";
  //config-----------------------------------------------
  const url = supertest('http://192.168.56.2:8080/firstAp');
  var account = testingDate.account;
  var author2Add =testingDate.author2Add;

  before('#login', (done) => {
    api.query(url, "", "/auth/login", account).then((body) => {
      expect(body.status).to.be.equal('success');
      token = body.token;
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#addAuthor', (done) => {
    api.query(url, token, "/author/addAuthor", author2Add).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });;
  });

  it('#check Add', (done) => {
    api.query(url, token, '/Author/searchAuthor', {}).then((body) => {
      expect(body.status).to.be.equal('success');
      body.data.forEach(data=>{
        if(data.name == author2Add.name){
          author2Add.id = data.id;
          console.log("id is "+author2Add.id);
          done();
        }
      });
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#modifyAuthor', (done) => {
    var author2Mod = {
      "id":author2Add.id,
      "connect":testingDate.author2Mod.connect
    };
    api.query(url, token, '/Author/modifyAuthor', author2Mod).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#check mod', (done) => {
    api.query(url, token, '/Author/searchAuthor', {}).then((body) => {
      expect(body.status).to.be.equal('success');
      body.data.forEach(data=>{
        if(data.name == author2Add.name){
          console.log(data);
          done();
        }
      });
    }).catch(err=>{
      console.log(err);
    });
  });

  it('#deleteAuthor', (done) => {
    api.query(url, token, "/author/deleteAuthor" , author2Add).then((body) => {
      expect(body.status).to.be.equal('success');
      done();
    }).catch(err=>{
      console.log(err);
    });
  });

});